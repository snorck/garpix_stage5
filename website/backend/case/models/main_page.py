from django.db import models
from garpix_page.models import BasePage


class MainPage(BasePage):
    template = "pages/main.html"

    phone_number_1 = models.CharField(max_length=16, verbose_name="Телефон", blank=True)
    main_title = models.CharField(max_length=96, verbose_name="Заголовок", blank=True)
    garpix_logo = models.ImageField(verbose_name="Логотип гарпикс", blank=True)
    main_logo = models.ImageField(verbose_name="Логотип основной(день сурка", blank=True)
    description_logo = models.TextField(verbose_name="описание нпротив лого", blank=True)
    main_background = models.ImageField(verbose_name="MAIN Background photo", blank=True)

    menu_item_one = models.CharField(max_length=96, verbose_name="Первый пункт меню", blank=True)
    menu_item_two = models.CharField(max_length=96, verbose_name="Второй пункт меню", blank=True)
    main_item_three = models.CharField(max_length=96, verbose_name="Третий пункт меню", blank=True)

    project_tasks_title = models.CharField(max_length=96, verbose_name="Заголовок Задачи проекта", blank=True)
    project_tasks_one = models.CharField(max_length=256, verbose_name="Задача проекта 1", blank=True)
    project_tasks_two = models.CharField(max_length=256, verbose_name="Задача проекта 2", blank=True)
    project_tasks_three = models.CharField(max_length=256, verbose_name="Задача проекта 3", blank=True)
    project_tasks_four = models.CharField(max_length=256, verbose_name="Задача проекта 4", blank=True)

    project_tasks_photo_one = models.ImageField(verbose_name="Задача проекта фото 1", blank=True)
    project_tasks_photo_two = models.ImageField(verbose_name="Задача проекта фото 2", blank=True)

    project_stage_title = models.CharField(max_length=96, verbose_name="Заголовок этапы проекта", blank=True)
    project_stage_one = models.CharField(max_length=96, verbose_name="Этап проекта 1", blank=True)
    project_stage_two = models.CharField(max_length=96, verbose_name="Этап проекта 2", blank=True)
    project_stage_three = models.CharField(max_length=96, verbose_name="Этап проекта 3", blank=True)
    project_stage_four = models.CharField(max_length=96, verbose_name="Этап проекта 4", blank=True)
    project_stage_five = models.CharField(max_length=96, verbose_name="Этап проекта 5", blank=True)

    project_stage_title_small = models.CharField(max_length=96, verbose_name="Заголовок этапы проекта(малый)",
                                                 blank=True)

    project_stage_description = models.TextField(verbose_name="описание особенностей проекта", blank=True)

    project_stage_photo_one = models.ImageField(verbose_name="Этапы проекта фото 1", blank=True)
    project_stage_photo_two = models.ImageField(verbose_name="Этапы проекта фото 2", blank=True)

    design_title = models.CharField(max_length=96, verbose_name="Заголовок дизайн", blank=True)
    design_description = models.TextField(verbose_name="описание дизайна", blank=True)

    design_photo_one = models.ImageField(verbose_name="Дизайн фото 1", blank=True)
    design_stage_photo_two = models.ImageField(verbose_name="Дизайн фото 2", blank=True)

    design_background = models.ImageField(verbose_name="Background photo", blank=True)

    integration_title = models.CharField(max_length=96, verbose_name="Заголовок Интеграции", blank=True)
    integration_one = models.CharField(max_length=96, verbose_name="Интеграция 1", blank=True)
    integration_two = models.CharField(max_length=96, verbose_name="Интеграция 2", blank=True)

    integration_photo_one = models.ImageField(verbose_name="Интеграция фото 1", blank=True)
    integration_photo_two = models.ImageField(verbose_name="Интеграция фото 2", blank=True)
    integration_photo_three = models.ImageField(verbose_name="Интеграция фото 3", blank=True)

    project_command_title = models.CharField(max_length=96, verbose_name="Заголовок Команда", blank=True)

    command_player_1 = models.CharField(max_length=96, verbose_name="Участник команды 1", blank=True)
    command_player_2 = models.CharField(max_length=96, verbose_name="Участник команды 2", blank=True)
    command_player_3 = models.CharField(max_length=96, verbose_name="Участник команды 3", blank=True)
    command_player_4 = models.CharField(max_length=96, verbose_name="Участник команды 4", blank=True)
    command_player_5 = models.CharField(max_length=96, verbose_name="Участник команды 5", blank=True)
    command_player_6 = models.CharField(max_length=96, verbose_name="Участник команды 6", blank=True)
    command_player_7 = models.CharField(max_length=96, verbose_name="Участник команды 7", blank=True)
    command_player_8 = models.CharField(max_length=96, verbose_name="Участник команды 8", blank=True)

    technology_title = models.CharField(max_length=96, verbose_name="Заголовок технологии", blank=True)
    technology_title_small = models.CharField(max_length=96, verbose_name="Заголовок технологии(малый)", blank=True)

    technology_item_1 = models.CharField(max_length=96, verbose_name="Технологии пункт 1", blank=True)
    technology_item_2 = models.CharField(max_length=96, verbose_name="Технологии пункт 2", blank=True)
    technology_item_3 = models.CharField(max_length=96, verbose_name="Технологии пункт 3", blank=True)

    technology_photo = models.ImageField(verbose_name="Технологии фото", blank=True)
    technology_background = models.ImageField(verbose_name="Background photo", blank=True)

    more_case_link = models.CharField(max_length=96, verbose_name="текст ссылки еще кейсы", blank=True)

    contacts_title = models.CharField(max_length=96, verbose_name="Заголовок контакты", blank=True)
    contacts_email = models.CharField(max_length=96, verbose_name="Email контакты", blank=True)

    contacts_address_1 = models.CharField(max_length=96, verbose_name="Адрес 1", blank=True)
    contacts_address_2 = models.CharField(max_length=96, verbose_name="Адрес 2", blank=True)
    contacts_address_3 = models.CharField(max_length=96, verbose_name="Адрес 3", blank=True)

    social_title_1 = models.CharField(max_length=96, verbose_name="социальная сеть 1 заголовок", blank=True)
    social_link_title_1 = models.CharField(max_length=96, verbose_name="социальная сеть 1 линк", blank=True)
    social_title_2 = models.CharField(max_length=96, verbose_name="социальная сеть 2 заголовок", blank=True)
    social_link_title_2 = models.CharField(max_length=96, verbose_name="социальная сеть 2 линк", blank=True)
    social_title_3 = models.CharField(max_length=96, verbose_name="социальная сеть 3 заголовок", blank=True)
    social_link_title_3 = models.CharField(max_length=96, verbose_name="социальная сеть 3 линк", blank=True)
    social_title_4 = models.CharField(max_length=96, verbose_name="социальная сеть 4 заголовок", blank=True)
    social_link_title_4 = models.CharField(max_length=96, verbose_name="социальная сеть 4 линк", blank=True)
    social_title_5 = models.CharField(max_length=96, verbose_name="социальная сеть 5 заголовок", blank=True)
    social_link_title_5 = models.CharField(max_length=96, verbose_name="социальная сеть 5 линк", blank=True)

    class Meta:
        verbose_name = "Main"
        verbose_name_plural = "Mains"
        ordering = ("-created_at",)
