from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CaseConfig(AppConfig):
    name = 'case'
    verbose_name = _('Case')
